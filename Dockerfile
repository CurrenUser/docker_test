FROM debian
RUN apt-get update && apt-get install -y \
			git \
			apache2 \
            php 
RUN cd / && mkdir src && cd /src && git init 
RUN git clone https://github.com/CurrenUser/EdgaPlanner.git \
	&& cp -R EdgaPlanner/php/api /var/www/html
EXPOSE 80 443
CMD ["apachectl", "-D", "FOREGROUND"]